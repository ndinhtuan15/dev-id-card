from imgaug import augmenters as iaa
import numpy as np
import cv2

class AugData(object):

    def __init__(self):
        pass

    #Heavy noise
    #images is list of image (w,h,channels) of (N, w, h, c)
    # output is similar so can train
    def augmentData(self, images):

        sometimes = lambda aug: iaa.Sometimes(0.5, aug)

        augumenters = iaa.Sequential([
            iaa.SomeOf(n=(0, 2), children=[
                iaa.Add(value=(-40, 40), per_channel=0.5),
                iaa.GaussianBlur(sigma=(0.0, 0.5)),
                iaa.AverageBlur(k=(3, 5)),
                iaa.MedianBlur(k=(3, 5)),
                iaa.AdditiveGaussianNoise(scale=0.05*255, per_channel=0.5),
                iaa.Dropout(p=(0, 0.2), per_channel=0.5),
                iaa.Grayscale(alpha=(0.0, 0.5)),
                iaa.ContrastNormalization((0.9, 1.1), per_channel=0.5),
                iaa.Affine(scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}, translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)} , rotate=(-3, 3), shear=(-5, 5))
            ], random_order=True),
            sometimes(iaa.CropAndPad(percent=(-0.05, 0.1), pad_mode=["constant", "edge"], pad_cval=(0, 128)))
        ], random_order=False)
        augumenters = augumenters.to_deterministic() #
        newDatas = augumenters.augment_images(images)
        return newDatas # -> dua vao train binh thuong

    #Less noise, more clean
    def addNoiseImages(self, imgs):

        noises = [
                iaa.Add(value=(-15, 15)),
                iaa.AdditiveGaussianNoise(scale=(0.0, 0.02*255)),
                iaa.Salt(p=(0, 0.005)),
                iaa.Pepper(p=(0, 0.005)),
                iaa.GaussianBlur(sigma=[0,0.002]),
                iaa.MedianBlur(k=[1, 3])]

        noise = iaa.SomeOf(n=(1), children=noises, random_order=False)
        seq = iaa.Sequential(noise)
        result = seq.augment_images(imgs)
        return result



import glob
import numpy as np

if __name__ == "__main__":

    img = cv2.imread("id.png")
    imgs = []
    for i in range(100):
        imgs.append(img)
    augData = AugData()
    newDatas = augData.addNoiseImages(imgs)
    print(len(newDatas))

    for i, img in enumerate(newDatas):
        cv2.imwrite("tuan_"+str(i)+".png", img)

