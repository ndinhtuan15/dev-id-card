## TO-DO

1. [x] Train with clean data => improve acc
2. [x] Create data less noise to train
3. [x] Train more epoch on 1 list dictionary
4. [x] Reduce random process in training phase
5. [x] Train from zero again
6. [] Choose the best model in folder model (based on ThinData)
7. [x] Read batch of word img for Recognizor instead of one word image=> Cannt do it because dont know width of image input
8. [x] Train for decrease loss to 0.123...
9. [] Evaluate with accuracy for while training. Save lowest acc clean and lowest noise, all
10. [x] ReadWord with batch of word instead of one word image. in Recognizor (like training)
11. [] While weights7217.h5 achive acc over 93% in Titan. but on Vison is 0.0% ????????
	Because need to add name for BatchNormalization instead of default name. If not layer BatchNorm will automatically gen name for BatchNorm and that name is not in model file and weight is not loaded => Use init value for that layer
12. 
	[] Train fine-tuning on Titan
	[x] Train from scratch on Vision

## Comment 

1. [] Train from scratch on Vision : achieve high accuracy sooner train fine-tune (Titan) and (loss Vison lower loss Titan when fine-tune)
2. [] Augument less noise data and more clean data achive high model on clean data(95 -> 97) but nosie data low : (92->87)
