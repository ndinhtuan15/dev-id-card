import os
import glob
from Recognizor import Recognizor
import cv2
import pickle
import json
import copy
import time
import numpy as np

class LoadDataNoiseClean(object):
    '''
    Class for loading data Noise and Clean for IQA data: 2 class (Noise and Clean)
    2 label folder for 2 class
    2 data folder for 2 class
    '''

    def __init__(self, top_dir, name_clean_label_dir, name_noise_label_dir, name_clean_data_dir, name_noise_data_dir, path_ratio_id, same_width=True, is_cropped=False, clean_data=True, change_width=None):

                assert os.path.exists(top_dir)
                assert os.path.exists(top_dir+"/"+name_clean_label_dir) and os.path.exists(top_dir+'/'+name_noise_label_dir)
                assert os.path.exists(top_dir+"/"+name_clean_data_dir) and os.path.exists(top_dir+"/"+name_noise_data_dir)

                if same_width is True:
                    self.width_img=None
                self.same_width = same_width
                self.change_width = change_width

                self.top_dir = top_dir
                self.path_clean_label_dir = self.top_dir + "/" + name_clean_label_dir
                self.path_noise_label_dir = self.top_dir + "/" + name_noise_label_dir
                self.path_clean_data_dir  = self.top_dir + "/" + name_clean_data_dir
                self.path_noise_data_dir  = self.top_dir + "/" + name_noise_data_dir

                # dictionary for label
                self.clean_labels = self.__get_labels(self.path_clean_label_dir)
                self.noise_labels = self.__get_labels(self.path_noise_label_dir)

                self.path_clean_imgs = glob.glob(self.path_clean_data_dir+'/*.jpg')
                self.path_noise_imgs = glob.glob(self.path_noise_data_dir+'/*.jpg')

                self.ratio_id = None
                self.clean_data = clean_data
                self.is_cropped=is_cropped

                with open(path_ratio_id) as fp:
                    ratios = pickle.load(fp)

                self.ratio_id = ratios[0]

        # dictionary for image data
                t1 = time.time()

                if clean_data:
                    print("Using clean data")
                    self.clean_imgs = self.__get_imgs(self.path_clean_imgs, self.ratio_id)
                    self.noise_imgs = None
                else:
                    print("Using noise data")
                    self.noise_imgs = self.__get_imgs(self.path_noise_imgs, self.ratio_id)
                    self.clean_imgs = None

                if clean_data:
                    self.num_clean_data = len(self.clean_imgs.keys())
                    self.num_noise_data = 0
                else:
                    self.num_clean_data = 0
                    self.num_noise_data = len(self.noise_imgs.keys())
                t2 = time.time()
                print("Time to load data: {}s".format(t2-t1))


    def get_num_clean_and_noise(self):
        return self.num_clean_data, self.num_noise_data

    def __get_imgs(self, path_imgs, ratio):

                result = dict()
                for path_img in path_imgs:
                        img = cv2.imread(path_img)
                        if self.is_cropped:
                            id_img = self.__crop_id(img, ratio)
                        else:
                            id_img=img

                        if self.same_width:
                            if self.width_img is None:
                                self.width_img = int(id_img.shape[1]  * 32.0 / id_img.shape[0])
                            if self.change_width is not None:
                                self.width_img = self.change_width

                        id_img = cv2.resize(id_img, (self.width_img, 32), interpolation=cv2.INTER_CUBIC)

                        result[path_img.split('/')[-1]] = id_img

                        #del img
                        #del id_img

                return result

    def __get_labels(self, path_label_dir):

                assert os.path.exists(path_label_dir)

                file_jsons = glob.glob(path_label_dir+'/*.json')
                result = dict()

                for file_json in file_jsons:
                    fp = open(file_json, 'r')
                    result.update(json.load(fp))

                return result

    def __crop_id(self, img, ratio):

                h_img, w_img, _ = img.shape
                ratio_x, ratio_y, ratio_x1, ratio_y1 = ratio

                x = int(w_img / ratio_x)
                y = int(h_img / ratio_y)
                x1 = int(w_img / ratio_x1)
                y1 = int(h_img / ratio_y1)

                return img[y:y1, x:x1]

    def get_clean_data(self):
        return self.clean_imgs, self.clean_labels

    def get_noise_data(self):
        return self.noise_imgs, self.noise_labels


    #clean=True will get list batchs clean data
    # clean=Fasle will get list batchs noise data
    def get_list_batchs_data(self, clean=True, batch_size=32):

        if clean:
            labels = self.clean_labels#dictionaty of labels
            datas = self.clean_imgs#dict of img
        else:
            labels = self.noise_labels
            datas = self.noise_imgs
        assert not (clean ^ self.clean_data), "Load only clean or noise data"
        assert batch_size < len(datas.keys())
        print len(datas.keys())
        list_batchs = []
        keys = self.clean_labels.keys()

        # get list batch of (batch_data, batch_label)
        i = 0
        for batch in range(int(len(datas.keys())/batch_size)):
            batch_key = datas.keys()[i:i+batch_size]
            batch_label = []
            for key in batch_key:
                batch_label.append(labels[key])

            list_batchs.append({"data": datas.values()[i:i+batch_size], "label":batch_label})
            i += batch_size

        if len(datas.keys()) % batch_size != 0:
                        remain_key = datas.keys()[i:]
                        remain_labels = []

                        for key in remain_key:
                            remain_labels.append(labels[key])
                        list_batchs.append({"data": datas.values()[i:], "label": remain_labels})

        return list_batchs

class ChoosingModel(object):

    def __init__(self, path_dir_model, path_dir_evaluation, clean_data=None, noise_data=None, batch_eval=False, list_clean_batch=None, list_noise_batch=None, num_clean_data=None, num_noise_data=None):

        if batch_eval:
                assert list_clean_batch is not None and list_noise_batch is not None
                assert num_clean_data is not None and num_noise_data is not None
                self.list_clean_batch = list_clean_batch
                self.list_noise_batch = list_noise_batch
                self.num_clean_data = num_clean_data
                self.num_noise_data = num_noise_data

                assert os.path.exists(path_dir_model)
                if not os.path.exists(path_dir_evaluation):
                    os.mkdir(path_dir_evaluation)

                print clean_data[0].values()[0].shape
                self.model_paths = glob.glob(path_dir_model+'/*.h5')
                self.name_models = [i.split('/')[-1] for i in self.model_paths]

                self.clean_eval_result = dict()
                self.noise_eval_result = dict()
                self.all_eval_result = dict()

                self.clean_data = clean_data
                self.noise_data = noise_data

    def evaluate_batch(self):

        for model_path in self.model_paths:
            print(model_path)
            recog = Recognizor(pathToWeightModel=model_path)
            model_name = model_path.split('/')[-1]





                #recog_noise = Recognizor(pathToWeightModel=model_path)
                #num_right_noise = 0
                #for batch_noise in self.list_noise_batch:
                #    batch_noise_img = batch_clean["data"]
                #    batch_label_img = batch_clean["label"]
                #    pred = recog_noise.read_batch_word(batch_noise_img)
                #
                #    num_right_noise += sum(np.array(pred)==np.array(batch_label_img))
                #
                #print("Acc noise: ", num_right_noise*1.0/self.num_noise_data)
                #
                #self.noise_eval_result[model_name] = num_right_noise*1.0/self.num_noise_data






            #recog_noise = Recognizor(pathToWeightModel=model_path)
            num_right_noise = 0
            for batch_noise in self.list_noise_batch:
                batch_noise_img = batch_noise["data"]
                batch_label_img = batch_noise["label"]
                pred = recog.read_batch_word(batch_noise_img)



                num_right_clean = 0
                for batch_clean in self.list_clean_batch:
                    batch_clean_img = batch_clean["data"]
                    batch_label_img = batch_clean["label"]
                    pred = recog.read_batch_word(batch_clean_img)

                    #print("pred: ", pred)
                    #print("label: ", batch_label_img)

                    num_right_clean += sum(np.array(pred)==np.array(batch_label_img))


                print ("Acc clean: ", num_right_clean*1.0/self.num_clean_data)
                self.clean_eval_result[model_name] = num_right_clean*1.0/self.num_clean_data

                num_right_noise = 0
                for batch_noise in self.list_noise_batch:
                    batch_noise_img = batch_noise["data"]
                    batch_label_img = batch_noise["label"]
                    pred = recog.read_batch_word(batch_noise_img)

                    num_right_noise += sum(np.array(pred)==np.array(batch_label_img))

                print("Acc noise: ", num_right_noise*1.0/self.num_noise_data)

                self.noise_eval_result[model_name] = num_right_noise*1.0/self.num_noise_data

                self.all_eval_result[model_name] = (num_right_noise+num_right_clean)*1.0/(self.num_clean_data+self.num_noise_data)

            #del recog_clean
            #del recog_noise

    def evaluate(self):

                h, old_width_img, _ = self.clean_data[0].values()[0].shape

                for model_path in self.model_paths:

                        print(model_path)
                        name_model = model_path.split('/')[-1]
                        recog = Recognizor(pathToWeightModel=model_path)

                        num_clean_img = len(self.clean_data[0].keys())
                        num_right_pred_clean = 0
                        #print(num_clean_img)
                        print(self.clean_data[1].keys())
                        for name_img in self.clean_data[0].keys():
                                img = self.clean_data[0][name_img]
                                label = self.clean_data[1][name_img]
                                pred = recog.readWord(img)#read_word_without_ctc(img)

                                if label == pred:
                                    num_right_pred_clean += 1
                                else:
                                    cv2.imwrite("l_{}_p_{}.jpg".format(label, pred), img)

                        acc_clean = num_right_pred_clean*1.0/num_clean_img
                        print("Acc Clean: ", acc_clean)

                        self.clean_eval_result[name_model] = acc_clean

                        num_noise_img = len(self.noise_data[0].keys())
                        num_right_pred_noise = 0
                        for name_img in self.noise_data[0].keys():
                                img = self.noise_data[0][name_img]
                                label = self.noise_data[1][name_img]
                                pred = recog.read_word_without_ctc(img)
                                pred1 = recog.readWord(img)

                                if label == pred1:
                                    num_right_pred_noise += 1
                                else:
                                    print ("Pred: ", pred, ". Label: ", label, "Pred1: ", pred1)

                        acc_noise = num_right_pred_noise*1.0/num_noise_img
                        print("Acc noise: ", acc_noise)

                        self.noise_eval_result[name_model] = acc_noise
                        self.all_eval_result[name_model] = (num_right_pred_clean + num_right_pred_noise)*1.0/(num_noise_img + num_clean_img)


    def save_clean_eval(self, name_clean_eval_file):
        with open(name_clean_eval_file, 'w') as fp:
            json.dump(self.clean_eval_result, fp)

    def save_noise_eval(self, name_noise_eval_file):

        with open(name_noise_eval_file, 'w') as fp:
            json.dump(self.noise_eval_result, fp)

    def save_all_eval(self, name_all_eval_file):

        with open(name_all_eval_file, 'w') as fp:
            json.dump(self.all_eval_result, fp)

if __name__ == "__main__":
    #choosingModel = ChoosingModel("model_id", "data_evaluation")
    loadData = LoadDataNoiseClean("DataThin", "CleanLabel", "NoiseLabel", "CleanData", "NoiseData", "ratios.txt", clean_data=True)
    num_clean_data, num_noise_data=loadData.get_num_clean_and_noise()
    list_clean_batchs = loadData.get_list_batchs_data(clean=True)
    print np.array(list_clean_batchs[1]["data"]).shape
    #list_noise_batchs = loadData.get_list_batchs_data(clean=False)

    #print(len(list_clean_batchs))
    #print(len(list_noise_batchs))

    #choosingModel= ChoosingModel("model_id", "choose_model_eval", loadData.get_clean_data(), loadData.get_noise_data(), list_clean_batch=list_clean_batchs, list_noise_batch=list_noise_batchs, num_clean_data=num_clean_data, num_noise_data=num_noise_data)

    #choosingModel.evaluate_batch()
    #choosingModel.evaluate()
    #choosingModel.save_clean_eval("clean_eval.json")
    #choosingModel.save_noise_eval("noise_eval.json")
    #choosingModel.save_all_eval("all_eval.json")

















