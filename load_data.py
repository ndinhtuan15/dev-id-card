import os
import cv2
import numpy as np
import random

class LoadRealData(object):

    def __init__(self, path_label, path_data, same_width=223):

        assert os.path.exists(path_label), "{} not exists.".format(path_label)
        assert os.path.exists(path_data), "{} not exists.".format(path_data)

        self.path_label = path_label
        self.path_data = path_data

        self.label_dict = np.load(path_label).item()
        self.data_dict = np.load(path_data).item()

        self.same_width = same_width

        print(len(self.label_dict.keys()))
        print(len(self.data_dict.keys()))

    def get_list_batch(self, batch_size=16):

        list_batchs = []
        keys = self.label_dict.keys()
        assert len(keys) == len(self.data_dict.keys())
        tmp = self.label_dict.keys()
        random.shuffle(tmp)
        datas = dict()

        for i in tmp:
            img = self.data_dict[i]
            if self.same_width is not None:
                img = cv2.resize(img, (self.same_width, 32), interpolation=cv2.INTER_CUBIC)
            datas[i] = img

        labels = self.label_dict

        # get list batch of (batch_data, batch_label)
        i = 0
        for batch in range(int(len(datas.keys())/batch_size)):

            batch_key = datas.keys()[i:i+batch_size]
            batch_label = []
            for key in batch_key:
                batch_label.append(labels[key])

            list_batchs.append({"data": datas.values()[i:i+batch_size], "label":batch_label})
            i += batch_size

        if len(datas.keys()) % batch_size != 0:
                        remain_key = datas.keys()[i:]
                        remain_labels = []

                        for key in remain_key:
                            remain_labels.append(labels[key])
                        list_batchs.append({"data": datas.values()[i:], "label": remain_labels})
        return list_batchs


if __name__=="__main__":

    load_data = LoadRealData(path_data="real_data_bin/good_imgs.npy", path_label="real_data_bin/good_labels.npy")
    list_batchs = load_data.get_list_batch()
    print len(list_batchs)

