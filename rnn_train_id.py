#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import cv2
import itertools
import codecs
import re
import datetime
import unicodedata
import cairocffi as cairo
import editdistance
import numpy as np
from scipy import ndimage
# import pylab
from keras import backend as K
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Input, Dense, Activation, Bidirectional, BatchNormalization
from keras.layers import Reshape, Lambda, Dropout
from keras.layers.merge import add, concatenate
from keras.models import Model
from keras.layers.recurrent import GRU, LSTM
from keras.optimizers import SGD, Adadelta, Adam
from keras.utils.data_utils import get_file
from keras.preprocessing import image
import keras.callbacks
import keras
import h5py
import cv2
import glob
import random
import copy
from AugData import AugData
import tensorflow as tf
from PIL import ImageFont, ImageDraw, Image
import sys
import json

OUT_DIR = 'model_id_clean'
OUT_BEST_MODEL= "model_id/best_loss"
OUT_BEST_NOISE_MODEL = 'model_id/best_noise_model'
OUT_BEST_CLEAN_MODEL = 'model_id/best_clean_model'
OUT_BEST_ALL_MODEL = 'model/best_all_model'
#
lossMin=1000
#
#if not os.path.exists(OUT_DIR):
#    os.mkdir(OUT_DIR)
#
#if not os.path.exists(OUT_BEST_NOISE_MODEL):
#    os.mkdir(OUT_BEST_NOISE_MODEL)
#
#if not os.path.exists(OUT_BEST_CLEAN_MODEL):
#     os.mkdir(OUT_BEST_CLEAN_MODEL)
#
#if not os.path.exists(OUT_BEST_ALL_MODEL):
#     os.mkdir(OUT_BEST_ALL_MODEL)

best_acc_clean = -1 # for save best model
best_acc_noise = -1
best_acc_all = -1

aguData = AugData()

histVal=[]

# character classes and matching regex filter
regex = r'^[a-z ]+$'
alphabet =  u"abcdđefghijklmnopqrstuvwxyzABCDĐEFGHIJKLMNOPQRSTUVWXYZ àảãáạăằẳẵắặâầẩẫấậèẻẽéẹêềểễếệìỉĩíịòỏõóọôồổỗốộơờởỡớợùủũúụưừửữứựỳỷỹýỵÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬÈẺẼÉẸÊỀỂỄẾỆÌỈĨÍỊÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢÙỦŨÚỤƯỪỬỮỨỰỲỶỸÝỴ0123456789'-.:,\/"

backs = []
files = [os.path.join("back", i) for i in os.listdir("back") if os.path.isfile(os.path.join("back", i))]
print files
for filename in files:
	back = cv2.imread(filename)
	#print back.shape
	h, w, _ = back.shape
	back = cv2.resize(back,dsize=None, fx=32.0/h, fy=32.0/h)
	#print back.shape
	backs.append(back)
	break
# for filename in glob.glob(r"back\*.jpg"):
# 	backs.append(cv2.imread(filename))
	#cv2.imshow("img", backs[-1])
	# cv2.waitKey(0)
##print(len(backs))

# fonts = [os.path.join("fonts", i) for i in os.listdir("fonts") if os.path.isfile(os.path.join("fonts", i))]
fonts = ['fonts/id.ttf', 'fonts/ARIALUNI.ttf', 'fonts/arial.ttf', 'fonts/id2.ttf', 'fonts/timesbd.ttf']
# fonts = ["fonts/font-times-new-roman.ttf"]
#print fonts

#np.random.seed(55)


# this creates larger "blotches" of noise which look
# more realistic than just adding gaussian noise
# assumes greyscale with pixels ranging from 0 to 1

def speckle(img):
	severity = np.random.uniform(0, 0.05)
	blur = ndimage.gaussian_filter(np.random.randn(*img.shape) * severity, 1)
	img_speck = (img + blur)
	img_speck[img_speck > 1] = 1
	img_speck[img_speck <= 0] = 0
	return img_speck


def paint_text(text, w, h, noise = 10, rotate=False, ud=False, multi_fonts=False):
	size = random.randint(20, 30)
	font = ImageFont.truetype(np.random.choice(fonts), size)
	ascent, descent = font.getmetrics()
	(width, baseline), (offset_x, offset_y) = font.font.getsize(text)
	height = ascent + descent
	max_shift_x = w - width
	max_shift_y = h - height
	# #print(size, width, height)
	while max_shift_x < 0 or max_shift_y < 0:
		size -= 1
		font = ImageFont.truetype(np.random.choice(fonts), size)
		ascent, descent = font.getmetrics()
		(width, baseline), (offset_x, offset_y) = font.font.getsize(text)
		height = ascent + descent
		max_shift_x = w - width
		max_shift_y = h - height
		# #print(size, width, height)

	top_left_x = np.random.randint(0, int(max_shift_x) + 1)
	top_left_y = np.random.randint(0, int(max_shift_y) + 1)
	back = copy.copy(backs[random.randint(0,len(backs)-1)][:,:w,:])
	img = Image.fromarray(back)
	draw = ImageDraw.Draw(img)
	color = np.ones(3) * random.randint(0, 100) + noise
	color -= np.random.randint(2 * noise, size = 3)
	color[color < 0] = 0
	b, g, r = color.astype(int)
	fill = (0, 0, 0, 0)
	i = random.randint(0, 10)
	if i < 3:
		fill = (b, g, r, 0)
	draw.text((top_left_x, top_left_y),  text, font = font, fill = fill)
	back = np.array(img)
	#cv2.imshow("id", back)
	#cv2.waitKey(0)

	# del img
	return back

def add_real_data(img_name, w, h):
	path = 'realData/'
	img = cv2.imread(path + img_name)
	new_h = random.randint(25, 32)
	new_w = int(1.0 * img.shape[1] * new_h / img.shape[0])
	max_shift_x = w - new_w
	max_shift_y = h - new_h
	while max_shift_x < 0 or max_shift_y < 0:
		new_h -= 1
		new_w = int(1.0 * img.shape[1] * new_h / img.shape[0])
		new_img = cv2.resize(img, (new_w, new_h), interpolation = cv2.INTER_CUBIC)
		max_shift_x = w - new_w
		max_shift_y = h - new_h
		# #print(size, width, height)
	new_img = cv2.resize(img, (new_w, new_h), interpolation = cv2.INTER_CUBIC)
	top_left_x = np.random.randint(0, int(max_shift_x) + 1)
	top_left_y = np.random.randint(0, int(max_shift_y) + 1)
	# back = copy.copy(backs[random.randint(0,len(backs)-1)][:,:w,:])
	back = np.zeros((h,w,3))
	back[top_left_y:top_left_y + new_h, top_left_x : top_left_x + new_w, :] = new_img
	return back

def shuffle_mats_or_lists(matrix_list, stop_ind=None):
	ret = []
	assert all([len(i) == len(matrix_list[0]) for i in matrix_list])
	len_val = len(matrix_list[0])
	if stop_ind is None:
		stop_ind = len_val
	assert stop_ind <= len_val

	a = list(range(stop_ind))
	np.random.shuffle(a)
	a += list(range(stop_ind, len_val))
	for mat in matrix_list:
		if isinstance(mat, np.ndarray):
			ret.append(mat[a])
		elif isinstance(mat, list):
			ret.append([mat[i] for i in a])
		else:
			raise TypeError('`shuffle_mats_or_lists` only supports '
							'numpy.array and list objects.')
	return ret


# Translation of characters to unique integer values
def text_to_labels(text):
	ret = []
	for char in text:
		label = alphabet.find(char)
		if label < 0:

			#label = alphabet.find(' ')
			#print("char:",int(char))
			continue
		ret.append(label)

	return ret


# Reverse translation of numerical classes back to characters
def labels_to_text(labels):
	ret = []
	for c in labels:
		if c == len(alphabet) or c == -1:  # CTC Blank
			ret.append("")
		else:
			ret.append(alphabet[c])
	return "".join(ret)


# only a-z and space..probably not to difficult
# to expand to uppercase and symbols

def is_valid_str(in_str):
	search = re.compile(alphabet).search
	return bool(search(in_str))


# Uses generator functions to supply train/test with
# data. Image renderings are text are created on the fly
# each time with random perturbations

class TextImageGenerator(keras.callbacks.Callback):

	def __init__(self, data_tuning, file_name, file_place, real_data, minibatch_size,
				 img_w, img_h, downsample_factor, val_split,
				 absolute_max_string_len = 20):

		self.minibatch_size = minibatch_size
		self.img_w = img_w
		self.img_h = img_h
		self.data_tuning = data_tuning
		self.file_place = file_place
		self.file_name = file_name
		self.real_data = real_data
		self.downsample_factor = downsample_factor
		self.val_split = val_split
		self.blank_label = self.get_output_size() - 1
		self.absolute_max_string_len = absolute_max_string_len

		self.ratio_data_tuning = 0.2
		self.ratio_place = 0.4
		self.ratio_name = 0.4
		self.minLen = None
		self.maxLen = None
		self.numIdDatas = None

	def get_output_size(self):
		return len(alphabet) + 1

	def setRangeLenIf(self, minLen, maxLen):
		self.minLen = minLen
		self.maxLen = maxLen

	def buildIdData(self, numDatas,max_string_len = None):

		assert self.minLen is not None
		assert self.maxLen is not None

		self.numIdDatas = numDatas
		self.num_words = numDatas

		self.string_list = [''] * self.numIdDatas
		tmp_string_list = []
		self.max_string_len = max_string_len
		self.Y_data = np.ones([self.numIdDatas, self.absolute_max_string_len]) * -1
		self.X_text = []
		self.Y_len = [0] * self.numIdDatas

		for i in range(self.numIdDatas):
			id = ""
			lenId = np.random.randint(self.minLen, self.maxLen)

			for j in range(lenId):
				id += str(np.random.randint(0, 10))
			tmp_string_list.append(id)

		random.shuffle(tmp_string_list)
		self.string_list = tmp_string_list
		#print len(self.string_list)

		for i, word in enumerate(self.string_list):
			#print("index: {}".format(i))
			# #print len(self.Y_len)
			self.Y_len[i] = len(word)
			self.Y_data[i, :len(word)] = text_to_labels(word)
			self.X_text.append(word)
		self.Y_len = np.expand_dims(np.array(self.Y_len), 1)

		self.cur_val_index = self.val_split
		self.cur_train_index = 0

        def build_real_data(self):

	    self.path_real_data = "real_data/train"
	    self.path_real_less_data = "real_less_clean_data/val"
	    self.real_label = json.load(open("real_data/train/label_train.json", "r"))
	    self.real_imgs = dict()
	    for key in self.real_label.keys():
		img = cv2.imread(self.path_real_data+'/'+key)
		self.real_imgs[key] = cv2.resize(img, (self.img_w, self.img_h))

	    self.len_real_data = len(self.real_label.keys())
	    self.cursor_real_data = 0
	    self.batch_size_real_data = 4

	    self.real_less_label = json.load(open("real_less_clean_data/val/label_test.json", 'r'))
	    self.real_less_imgs = dict()
	    for key in self.real_less_label.keys():
		img = cv2.imread(self.path_real_less_data+'/'+key)
		self.real_less_imgs[key] = cv2.resize(img, (self.img_w, self.img_h))
            self.len_real_less_data = len(self.real_less_label.keys())
            self.cursor_real_less_data = 0
            self.batch_size_real_less_data = 8

	# num_words can be independent of the epoch size due to the use of generators
	# as max_string_len grows, num_words can grow
	def build_word_list(self, num_words, max_string_len=None):
		assert max_string_len <= self.absolute_max_string_len
		assert num_words % self.minibatch_size == 0
		assert (self.val_split * num_words) % self.minibatch_size == 0
		self.num_words = num_words
		self.string_list = [''] * self.num_words
		tmp_string_list = []
		self.max_string_len = max_string_len
		self.Y_data = np.ones([self.num_words, self.absolute_max_string_len]) * -1
		self.X_text = []
		self.Y_len = [0] * self.num_words

		# monogram file is sorted by frequency in english speech
		with codecs.open(self.data_tuning, mode='r', encoding='utf-8', errors='ignore') as f:
			lines = f.readlines()
			random.shuffle(lines)
			for line in lines:

					if len(tmp_string_list) == int(self.num_words*self.ratio_data_tuning):
						break
					word = ""
					for ch in line:
						if alphabet.find(ch) >= 0:
							word+= ch
					word = word.split()
					if len(word) >= 1 and (max_string_len == -1 or max_string_len is None or len(word) <= max_string_len):
						tmp_string_list.append(" ".join(word))

		with codecs.open(self.file_name, mode='r', encoding='utf-8', errors='ignore') as f:
			#print "get data in name"
			lines = f.readlines()
			random.shuffle(lines)

			for line in lines:
					if len(tmp_string_list) == int(self.num_words*(self.ratio_data_tuning + self.ratio_name)):
						break
					word = ""
					for ch in line:
						if alphabet.find(ch) >= 0:
							word+= ch
					word = word.split()
					if len(word) >= 1 and (max_string_len == -1 or max_string_len is None or len(word) <= max_string_len):
						tmp_string_list.append(" ".join(word))
		#print "len lib:", len(tmp_string_list)

		with codecs.open(self.file_place, mode='r', encoding='utf-8', errors='ignore') as f:
			#print "get data in file place"
			lines = f.readlines()
			random.shuffle(lines)

			for line in lines:

					if len(tmp_string_list) == self.num_words:
						break
					word = ""
					for ch in line:
						if alphabet.find(ch) >= 0:
							word+= ch
					word = word.split()
					if len(word) >= 1 and (max_string_len == -1 or max_string_len is None or len(word) <= max_string_len):
						tmp_string_list.append(" ".join(word))
		#print "len lib: ", len(tmp_string_list)

		with codecs.open(self.real_data, mode='r', encoding='utf-8', errors='ignore') as f:
				lines = f.readlines()
				random.shuffle(lines)
				self.real_img_path = []
				self.real_text = []
				self.real_data = np.ones([len(lines), self.absolute_max_string_len]) * -1
				self.real_len = [0] * len(lines)
				i = 0
				for line in lines:

					line = line.replace("\n", "")
					path, text = line.split(r"|")
					# #print(i, path, text)
					self.real_img_path.append(path)
					self.real_text.append(text)
					self.real_len[i] = len(text)
					self.real_data[i, :len(text)] = text_to_labels(text)
					i += 1

		if len(tmp_string_list) < self.num_words:
			##print("error", len(tmp_string_list), self.num_words)
			#print  self.num_words
			#print len(tmp_string_list)
			raise IOError('Could not pull enough words from supplied monogram and bigram files. ')


		random.shuffle(tmp_string_list)
		self.string_list = tmp_string_list
		#print len(self.string_list)
		for i, word in enumerate(self.string_list):
			# #print i
			# #print len(self.Y_len)
			self.Y_len[i] = len(word)
			self.Y_data[i, :len(word)] = text_to_labels(word)
			self.X_text.append(word)
		self.Y_len = np.expand_dims(np.array(self.Y_len), 1)

		self.cur_val_index = self.val_split
		self.cur_train_index = 0

	# each time an image is requested from train/val/test, a new random
	# painting of the text is performed
	def get_batch(self, index, size, train):
		# width and height are backwards from typical Keras convention
		# because width is the time dimension when it gets fed into the RNN

		X_data = np.zeros([size, self.img_h, self.img_w, 3])
		labels = np.ones([size, self.absolute_max_string_len])
		input_length = np.zeros([size, 1])
		label_length = np.zeros([size, 1])
		source_str = []
		images = []

		real_X_data = np.zeros([self.batch_size_real_data, self.img_h, self.img_w, 3])
		real_labels = np.ones([self.batch_size_real_data, self.absolute_max_string_len])*-1
		real_input_length = np.zeros([self.batch_size_real_data, 1])
		real_label_length = np.zeros([self.batch_size_real_data, 1])
		real_source_str = []
		real_images = []

		for i in range(self.batch_size_real_data):

		    name_img = self.real_label.keys()[self.cursor_real_data + i]
                    real_labels[i,:len(self.real_label[name_img])] = text_to_labels(self.real_label[name_img])
                    img = self.real_imgs[name_img]
                    real_images.append(img)
                    real_X_data[i] = img
                    real_source_str.append(self.real_label[name_img])
                    real_label_length[i] = len(self.real_label[name_img])
                    real_input_length[i] = self.img_w/self.downsample_factor-1

		#real_label_length = np.expand_dims(np.array(real_label_length), 1)

		real_less_X_data = np.zeros([self.batch_size_real_less_data, self.img_h, self.img_w, 3])
                real_less_labels = np.ones([self.batch_size_real_less_data, self.absolute_max_string_len])*-1
                real_less_input_length = np.zeros([self.batch_size_real_less_data, 1])
                real_less_label_length = np.zeros([self.batch_size_real_less_data, 1])
                real_less_source_str = []
                real_less_images = []

		for i in range(self.batch_size_real_less_data):
		    name_img = self.real_less_label.keys()[self.cursor_real_less_data + i]
                    real_less_labels[i,:len(self.real_less_label[name_img])] = text_to_labels(self.real_less_label[name_img])
		    img = self.real_less_imgs[name_img]
		    real_less_images.append(img)
		    real_less_X_data[i] = img
		    real_less_source_str.append(self.real_less_label[name_img])
		    real_less_label_length[i] = len(self.real_less_label[name_img])
		    real_less_input_length[i] = self.img_w/self.downsample_factor-1

		#real_less_label_length = np.expand_dims(np.array(real_less_label_length), 1)

		for i in range(size):

			# Mix in some blank inputs.  This seems to be important for
			# achieving translational invariance
			if train and i > size - 4:
				img = self.paint_func('')
				X_data[i] = img
				images.append(img)
				labels[i, 0] = self.blank_label
				input_length[i] = self.img_w / self.downsample_factor - 1
				label_length[i] = 1
				source_str.append('')
			else:
				img = self.paint_func(self.X_text[index + i])
				images.append(img)
				X_data[i] = img
				labels[i, :] = self.Y_data[index + i]
				input_length[i] = self.img_w / self.downsample_factor - 1
				label_length[i] = self.Y_len[index + i]
				source_str.append(self.X_text[index + i])

		# print "Test val : ", len(images)
		#Add real data
		# for i in range(2):

		# 	index = random.randint(0, len(self.real_img_path)-1)
		# 	img = add_real_data(self.real_img_path[index], self.img_w, self.img_h)
		# 	img = img.astype(np.uint8)
		# 	images.append(img)
		# 	labels[size-i-1, :] = self.real_data[index]
		# 	label_length[size - i-1] = self.real_len[index]
		# 	input_length[size - i-1] = self.img_w / self.downsample_factor - 1
		# 	source_str.append(self.real_text[index])


		#concatenate
		X_data = np.concatenate((X_data, real_X_data))
		#X_data = np.concatenate((X_data, real_less_X_data))
		labels = np.concatenate((labels, real_labels))
		#labels = np.concatenate((labels, real_less_labels))
		input_length = np.concatenate((input_length, real_input_length))
		#input_length = np.concatenate((input_length, real_less_input_length))
		label_length = np.concatenate((label_length, real_label_length))
		#label_length = np.concatenate((label_length, real_less_label_length))
		source_str += real_source_str
		#source_str += real_less_source_str
		images += real_images
		#images += real_less_images

                #raise SystemExit
                # Test show batch_img
                #print labels.shape
                #for label, img in zip(source_str,images):
                #    cv2.imwrite("test_train/{}.jpg".format(label), img)
		tmp = random.randint(1, 10)
		if tmp <= 3:
			images = aguData.addNoiseImages(images)
		X_data = np.asarray(images); print X_data.shape

		inputs = {'the_input': X_data,
				  'the_labels': labels,
				  'input_length': input_length,
				  'label_length': label_length,
				  'source_str': source_str,  # used for visualization only
				  'images': images
				  }
		outputs = {'ctc': np.zeros([size+self.batch_size_real_data])}  # dummy data for dummy loss function
		return (inputs, outputs)

	def next_train(self):
                print "ASDASda"
		while 1:
                        print "dasdasd"
			ret = self.get_batch(self.cur_train_index, self.minibatch_size, train=True)
			self.cur_train_index += self.minibatch_size
                        print self.cur_train_index
			if self.cur_train_index >= self.val_split:
				self.cur_train_index = 0
				#print("6", len(self.X_text))
				(self.X_text, self.Y_data, self.Y_len) = shuffle_mats_or_lists(
					[self.X_text, self.Y_data, self.Y_len], self.val_split)
			# 	print("7", len(self.X_text))
			# print "ReT: ", len(ret[0]['the_input'])
                        self.cursor_real_data += self.batch_size_real_data
                        self.cursor_real_less_data += self.batch_size_real_less_data

                        if self.cursor_real_data >= self.len_real_data-self.batch_size_real_data:
                            self.cursor_real_data = self.cursor_real_data % self.batch_size_real_data
                        if self.cursor_real_less_data >= self.len_real_less_data-self.batch_size_real_less_data:
                            self.cursor_real_less_data = self.cursor_real_less_data % self.batch_size_real_less_data
			yield ret

	def next_val(self):
		print "SHit"
		while 1:
			print ("vallll ", self.cur_val_index)
			ret = self.get_batch(self.cur_val_index, self.minibatch_size, train=False)
			self.cur_val_index += self.minibatch_size
			if self.cur_val_index >= self.num_words:
				self.cur_val_index = self.val_split
			yield ret

	def on_train_begin(self, logs={}):
		# self.build_word_list(24000, -1)
                self.buildIdData(16000, -1)
		self.build_real_data()
		self.paint_func = lambda text: paint_text(text, self.img_w, self.img_h,
												  rotate=False, ud=False, multi_fonts=False)

	def on_epoch_begin(self, epoch, logs={}):
		K.set_learning_phase(True)
		# rebind the paint function to implement curriculum learning
		if 10 <= epoch < 20:
			self.paint_func = lambda text: paint_text(text, self.img_w, self.img_h,
													  rotate=False, ud=True, multi_fonts=False)
		elif 20 <= epoch < 30:
			self.paint_func = lambda text: paint_text(text, self.img_w, self.img_h,
													  rotate=False, ud=True, multi_fonts=True)
		elif epoch >= 30:
			self.paint_func = lambda text: paint_text(text, self.img_w, self.img_h,
													  rotate=True, ud=True, multi_fonts=True)
		max_len = -1
		num_word = 24000
		rate = 0.2
		if epoch % 99 == 0 and epoch != 0:
	    	# self.build_word_list(num_word, max_len)
			self.buildIdData(16000, -1)



# the actual loss calc occurs here despite it not being
# an internal Keras loss function

def ctc_lambda_func(args):
	y_pred, labels, input_length, label_length = args
	return K.ctc_batch_cost(labels, y_pred, input_length, label_length)

def ctc_decoder(args):
	y_pred, input_length = args
	seq_len = tf.squeeze(input_length, axis = 1)
	return K.ctc_decode(y_pred = y_pred, input_length= seq_len, greedy= True, beam_width=100, top_paths = 1)[0]


# For a real OCR application, this should be beam search with a dictionary
# and language model.  For this example, best path is sufficient.

def decode_batch(test_func, word_batch):
	out = test_func([word_batch, 0])[0]
	ret = []
	for j in range(out.shape[0]):
		out_best = list(np.argmax(out[j], 1))
		out_best = [k for k, g in itertools.groupby(out_best)]
		outstr = labels_to_text(out_best)
		ret.append(outstr)
	return ret


class VizCallback(keras.callbacks.Callback):

	def __init__(self, run_name, test_func, text_img_gen, num_display_words=12, decode_func = None):
		self.test_func = test_func
		self.decode_func = decode_func
		self.output_dir = OUT_DIR +  "/" + run_name
		self.bestOutDir = OUT_BEST_MODEL
		##print (self.output_dir, 'test')
		self.text_img_gen = text_img_gen
		self.num_display_words = num_display_words
		self.bestLoss = lossMin
		self.currLoss = None
		if not os.path.exists(self.output_dir):
			os.makedirs(self.output_dir)
                if not os.path.exists(self.bestOutDir):
                    os.makedirs(self.bestOutDir)

	def show_edit_distance(self, num):
		num_left = num
		mean_norm_ed = 0.0
		mean_ed = 0.0
		#print "Show edit distance"
		while num_left > 0:
			word_batch = next(self.text_img_gen)[0]
			num_proc = min(word_batch['the_input'].shape[0], num_left)
			# #print("num_proc", num_proc)
			decoded_res = decode_batch(self.test_func, word_batch['the_input'][0:num_proc])
			for j in range(num_proc):
				edit_dist = editdistance.eval(decoded_res[j], word_batch['source_str'][j])
				mean_ed += float(edit_dist)
				mean_norm_ed += float(edit_dist) / len(word_batch['source_str'][j])
			num_left -= num_proc
		mean_norm_ed = mean_norm_ed / num
		mean_ed = mean_ed / num
		self.currLoss = mean_ed
		histVal.append(mean_ed)
		print('\nOut of %d samples:  Mean edit distance: %.3f Mean normalized edit distance: %0.3f'
			  % (num, mean_ed, mean_norm_ed))

	def on_epoch_end(self, epoch, logs={}):
		K.set_learning_phase(False)
		print "On epoch end"


		self.show_edit_distance(1000)
		if epoch % 13 == 0:
			word_batch = next(self.text_img_gen)[0]
			res = decode_batch(self.test_func, word_batch['the_input'][0:self.num_display_words])
			predicted = self.test_func([word_batch['the_input'][:self.num_display_words], 0])[0]
			# #print(predicted.shape)
			ctc_res = self.decode_func([predicted, word_batch["input_length"][:self.num_display_words]])[0]
			for i in range(self.num_display_words):
				print("Truth = '%s' \tDecoded = '%s'\tCtc_decoder = '%s'" % (word_batch['source_str'][i], res[i], labels_to_text(ctc_res[i])))


		self.model.save_weights(os.path.join(self.output_dir, 'weights%02d.h5' % (epoch)))
		# if self.bestLoss > self.currLoss:
		# 	self.model.save_weights(os.path.join(self.bestOutDir, 'weights%02d.h5' % (epoch)))
		# 	self.bestLoss = self.currLoss


def train(run_name, start_epoch, stop_epoch, img_w, fine_tune_part = False):
	# Input Parameters
	img_h = 32
	words_per_epoch = 16000
	val_split = 0.2
	val_words = int(words_per_epoch * (val_split))

	input_shape = (img_h, img_w, 3)
	minibatch_size = 32
	pool_size = 2
	img_gen = TextImageGenerator(data_tuning='data.txt', file_name='new_name.txt',
								file_place='words.txt', real_data='realData.txt',
								 minibatch_size=minibatch_size,
								 img_w=img_w,
								 img_h=img_h,
								 downsample_factor=(pool_size ** 3),
								 val_split=words_per_epoch - val_words
								 )
	img_gen.setRangeLenIf(8, 12)
	input_data = Input(name='the_input', shape=input_shape, dtype='float32')
	inner = Conv2D(64, (3,3),strides = (1,1), padding='same', activation="relu", kernel_initializer='he_normal', name='conv1')(input_data)
	inner = MaxPooling2D(pool_size=(2, 2), strides=(2,2), name='max1')(inner)
	inner = Conv2D(128, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv2')(inner)
	inner = MaxPooling2D(pool_size=(2, 2),strides=(2,2), name='max2')(inner)
	inner = Conv2D(256, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv3')(inner)
	inner = Conv2D(256, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv4')(inner)
	inner = MaxPooling2D(pool_size=(2, 2),strides=(2,2), name='max3')(inner)
	inner = Dropout(0.2, name = "dropout1")(inner)
	inner = Conv2D(512, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv5')(inner)
	inner = BatchNormalization()(inner)
	inner = Dropout(0.2, name = "dropout2")(inner)
	inner = Conv2D(512, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv6')(inner)
	inner = BatchNormalization()(inner)
	inner = MaxPooling2D(pool_size=(2, 1),strides=(2,1), name='max4')(inner)

	CNN_out = Conv2D(512, (2,2),strides = (1,1),activation="relu", kernel_initializer='he_normal',name='conv7')(inner)

	# Model(inputs=input_data, outputs=inner).summary()
	conv_to_rnn_dims = (img_w // (2 ** 3) - 1,512)

	inner = Reshape(target_shape=conv_to_rnn_dims, name='reshape')(CNN_out)

	lstm1 = LSTM(256, return_sequences=True, kernel_initializer="he_normal", name='lstm1', dropout=0.4)(inner)
	lstm1b = LSTM(256, return_sequences=True, go_backwards=True, kernel_initializer="he_normal", name='lstm1b', dropout=0.4)(inner)

	merge_concat = concatenate([lstm1, lstm1b])

	inner = Dense(img_gen.get_output_size(), kernel_initializer="he_normal", name="Dense")(merge_concat)
	y_pred = Activation("softmax", name="softmax")(inner)

	Model(inputs=input_data, outputs=y_pred).summary()

	labels = Input(name='the_labels', shape=[img_gen.absolute_max_string_len], dtype='float32')
	input_length = Input(name='input_length', shape=[1], dtype='int64')
	label_length = Input(name='label_length', shape=[1], dtype='int64')
	# Keras doesn't currently support loss funcs with extra parameters
	# so CTC loss is implemented in a lambda layer
	loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([y_pred, labels, input_length, label_length])

	# clipnorm seems to speeds up convergence
	#sgd = SGD(lr=0.0003, decay=1e-8, momentum=0.9, nesterov=True, clipnorm=5)
	#ada = Adadelta(decay=1e-8)
        adam = Adam()

	model = Model(inputs=[input_data, labels, input_length, label_length], outputs=loss_out)

	# the loss calc occurs elsewhere, so use a dummy lambda func for the loss
	model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=adam)
	if start_epoch > 0:
	    weight_file = os.path.join(OUT_DIR, os.path.join(run_name, 'weights%02d.h5' % (start_epoch-1)))


            # If fine_tune_part == True->just load part of model
	    if not fine_tune_part :
                model.load_weights(weight_file, True)
	    else:
                CNN = Model(input_data, CNN_out)
                CNN.load_weights(weight_file, True)

	test_func = K.function([input_data, K.learning_phase()], [y_pred])
	decoder = Lambda(ctc_decoder, output_shape = [None, ], name="decoder")([y_pred, input_length])
	decode_func = K.function([y_pred, input_length], [decoder])

        # img_gen.next_val() call in 2 place so will at one time 2 place also call next_val() -> no data to val
	viz_cb = VizCallback(run_name, test_func, img_gen.next_val(), decode_func = decode_func)

	model.fit_generator(generator=img_gen.next_train(),
						steps_per_epoch=(words_per_epoch - val_words) // minibatch_size,
						epochs=stop_epoch,
						validation_data=img_gen.next_val(),
						validation_steps=val_words // minibatch_size,
						callbacks=[viz_cb, img_gen],
						initial_epoch=start_epoch)

from Recognizor import Recognizor
import pickle
if __name__ == '__main__':
	run_name = "" #"20180226151024" #datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        #for i in range(100):
        #    img = paint_text("100", 342, 32)
        #    cv2.imwrite("tuan_"+str(i)+".png", img)
	#train(run_name, 106, 200, 342)
        train(run_name, 0, 300, 342)
	f = open("histVal3.txt", 'w')
	pickle.dump(histVal, f)
        #img_h = 32
        #words_per_epoch = 16000
        #val_split = 0.2
        #val_words = int(words_per_epoch * (val_split))

        #input_shape = (img_h, 342, 3)
        #minibatch_size = 32
        #pool_size = 2

        #img_gen = TextImageGenerator(data_tuning='data.txt', file_name='new_name.txt',
                                                                # file_place='words.txt', real_data='realData.txt',
                                                                  # minibatch_size=minibatch_size,
                                                                  # img_w=342,
                                                                  # img_h=img_h,
                                                                  # downsample_factor=(pool_size ** 3),
                                                                  # val_split=words_per_epoch - val_words
                                                                  # )
        #while 1:
         #   img_gen.next_val()

