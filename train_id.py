#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import cv2
import itertools
import codecs
import re
import datetime
import unicodedata
import cairocffi as cairo
import editdistance
import numpy as np
from scipy import ndimage
# import pylab
from keras import backend as K
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Input, Dense, Activation, Bidirectional, BatchNormalization
from keras.layers import Reshape, Lambda, Dropout
from keras.layers.merge import add, concatenate
from keras.models import Model
from keras.layers.recurrent import GRU, LSTM
from keras.optimizers import SGD, Adadelta, Adam
from keras.utils.data_utils import get_file
from keras.preprocessing import image
import keras.callbacks
import keras
import h5py
import cv2
import glob
import random
import copy
from AugData import AugData
import tensorflow as tf
from PIL import ImageFont, ImageDraw, Image
import sys
import json
from choose_model import LoadDataNoiseClean
from imgaug import augmenters as iaa
from load_data import LoadRealData

noise_aug = iaa.AdditiveGaussianNoise(scale=(0.05*255, 0.1*255))
OUT_DIR = 'model_id_fake_clean'
OUT_BEST_MODEL= "model_id/best_loss"
OUT_BEST_NOISE_MODEL = 'model_id/best_noise_model'
OUT_BEST_CLEAN_MODEL = 'model_id/best_clean_model'
OUT_BEST_ALL_MODEL = 'model/best_all_model'
#
lossMin=1000
#
#if not os.path.exists(OUT_DIR):
#    os.mkdir(OUT_DIR)
#
#if not os.path.exists(OUT_BEST_NOISE_MODEL):
#    os.mkdir(OUT_BEST_NOISE_MODEL)
#
#if not os.path.exists(OUT_BEST_CLEAN_MODEL):
#     os.mkdir(OUT_BEST_CLEAN_MODEL)
#
#if not os.path.exists(OUT_BEST_ALL_MODEL):
     #os.mkdir(OUT_BEST_ALL_MODEL)

best_acc_clean = -1 # for save best model
best_acc_noise = -1
best_acc_all = -1

aguData = AugData()

hist_val_clean_loss = []
hist_val_noise_loss = []
hist_train_clean_loss = []
hist_train_noise_loss = []
acc_test_clean = []
acc_test_noise = []
acc_train_noise = []
acc_train_clean = []

# character classes and matching regex filter
regex = r'^[a-z ]+$'
alphabet =  "0123456789"
backs = []
files = [os.path.join("back", i) for i in os.listdir("back") if os.path.isfile(os.path.join("back", i))]
print files
for filename in files:
	back = cv2.imread(filename)
	#print back.shape
	h, w, _ = back.shape
	back = cv2.resize(back,dsize=None, fx=32.0/h, fy=32.0/h)
	#print back.shape
	backs.append(back)
	break

# fonts = [os.path.join("fonts", i) for i in os.listdir("fonts") if os.path.isfile(os.path.join("fonts", i))]
fonts = ['fonts/id.ttf', 'fonts/ARIALUNI.ttf', 'fonts/arial.ttf', 'fonts/id2.ttf', 'fonts/timesbd.ttf']
# fonts = ["fonts/font-times-new-roman.ttf"]
#print fonts

#np.random.seed(55)


# this creates larger "blotches" of noise which look
# more realistic than just adding gaussian noise
# assumes greyscale with pixels ranging from 0 to 1

def speckle(img):
	severity = np.random.uniform(0, 0.05)
	blur = ndimage.gaussian_filter(np.random.randn(*img.shape) * severity, 1)
	img_speck = (img + blur)
	img_speck[img_speck > 1] = 1
	img_speck[img_speck <= 0] = 0
	return img_speck


def paint_text(text, w, h, noise = 10, rotate=False, ud=False, multi_fonts=False):
	size = random.randint(20, 30)
	font = ImageFont.truetype(np.random.choice(fonts), size)
	ascent, descent = font.getmetrics()
	(width, baseline), (offset_x, offset_y) = font.font.getsize(text)
	height = ascent + descent
	max_shift_x = w - width
	max_shift_y = h - height
	# #print(size, width, height)
	while max_shift_x < 0 or max_shift_y < 0:
		size -= 1
		font = ImageFont.truetype(np.random.choice(fonts), size)
		ascent, descent = font.getmetrics()
		(width, baseline), (offset_x, offset_y) = font.font.getsize(text)
		height = ascent + descent
		max_shift_x = w - width
		max_shift_y = h - height
		# #print(size, width, height)

	top_left_x = np.random.randint(0, int(max_shift_x) + 1)
	top_left_y = np.random.randint(0, int(max_shift_y) + 1)
	back = copy.copy(backs[random.randint(0,len(backs)-1)][:,:w,:])
	img = Image.fromarray(back)
	draw = ImageDraw.Draw(img)
	color = np.ones(3) * random.randint(0, 100) + noise
	color -= np.random.randint(2 * noise, size = 3)
	color[color < 0] = 0
	b, g, r = color.astype(int)
        fill = (0, 0, 0, 0)
        i = random.randint(0, 10)
        if i < 3:
            fill = (b, g, r, 0)
	draw.text((top_left_x, top_left_y),  text, font = font, fill = fill)
	back = np.array(img)
	#cv2.imshow("id", back)
	#cv2.waitKey(0)

	# del img
	return back

def add_real_data(img_name, w, h):
	path = 'realData/'
	img = cv2.imread(path + img_name)
	new_h = random.randint(25, 32)
	new_w = int(1.0 * img.shape[1] * new_h / img.shape[0])
	max_shift_x = w - new_w
	max_shift_y = h - new_h
	while max_shift_x < 0 or max_shift_y < 0:
		new_h -= 1
		new_w = int(1.0 * img.shape[1] * new_h / img.shape[0])
		new_img = cv2.resize(img, (new_w, new_h), interpolation = cv2.INTER_CUBIC)
		max_shift_x = w - new_w
		max_shift_y = h - new_h
		# #print(size, width, height)
	new_img = cv2.resize(img, (new_w, new_h), interpolation = cv2.INTER_CUBIC)
	top_left_x = np.random.randint(0, int(max_shift_x) + 1)
	top_left_y = np.random.randint(0, int(max_shift_y) + 1)
	# back = copy.copy(backs[random.randint(0,len(backs)-1)][:,:w,:])
	back = np.zeros((h,w,3))
	back[top_left_y:top_left_y + new_h, top_left_x : top_left_x + new_w, :] = new_img
	return back

def shuffle_mats_or_lists(matrix_list, stop_ind=None):
	ret = []
	assert all([len(i) == len(matrix_list[0]) for i in matrix_list])
	len_val = len(matrix_list[0])
	if stop_ind is None:
		stop_ind = len_val
	assert stop_ind <= len_val

	a = list(range(stop_ind))
	np.random.shuffle(a)
	a += list(range(stop_ind, len_val))
	for mat in matrix_list:
		if isinstance(mat, np.ndarray):
			ret.append(mat[a])
		elif isinstance(mat, list):
			ret.append([mat[i] for i in a])
		else:
			raise TypeError('`shuffle_mats_or_lists` only supports '
							'numpy.array and list objects.')
	return ret


# Translation of characters to unique integer values
def text_to_labels(text):
	ret = []
	for char in text:
		label = alphabet.find(char)
		if label < 0:

			#label = alphabet.find(' ')
			#print("char:",int(char))
			continue
		ret.append(label)

	return ret


# Reverse translation of numerical classes back to characters
def labels_to_text(labels):
	ret = []
	for c in labels:
		if c == len(alphabet) or c == -1:  # CTC Blank
			ret.append("")
		else:
			ret.append(alphabet[c])
	return "".join(ret)


# only a-z and space..probably not to difficult
# to expand to uppercase and symbols

def is_valid_str(in_str):
	search = re.compile(alphabet).search
	return bool(search(in_str))


# Uses generator functions to supply train/test with
# data. Image renderings are text are created on the fly
# each time with random perturbations

class TextImageGenerator(keras.callbacks.Callback):

    def on_train_begin(self, logs={}):
                K.set_learning_phase(True)
		print("Train begin")

    def on_epoch_begin(self, epoch, logs={}):
		K.set_learning_phase(True)
		#print epoch

    def __init__(self, ratio_val, ratio_real_val, img_w, img_h, batch_size, batch_real_size, downsample_factor, clean=True, absolute_max_len=20, good=True):

		self.index_val_batch = None
		self.absolute_max_len=absolute_max_len
		self.img_w = img_w
		self.img_h = img_h
		self.downsample_factor = downsample_factor

		if clean:
			self.load_data = LoadDataNoiseClean("DataThin", "CleanLabel", "NoiseLabel", "CleanData", "NoiseData", "ratios.txt", change_width=img_w)
		else:
			self.load_data = LoadDataNoiseClean("DataThin", "CleanLabel", "NoiseLabel", "CleanData", "NoiseData", "ratios.txt", clean_data=False, change_width=img_w)

                if good:
                    self.real_data = LoadRealData(path_data="real_data_bin/good_imgs.npy", path_label="real_data_bin/good_labels.npy", same_width=img_w)
                else:
                    self.real_data = LoadRealData(path_data="real_data_bin/bad_imgs.npy", path_label="real_data_bin/bad_labels.npy", same_width=img_w)

		self.list_batchs = self.load_data.get_list_batchs_data(clean=clean, batch_size=batch_size-batch_real_size)
		self.index_val_batch = int(len(self.list_batchs)*ratio_val)
		print("Index val : ", self.index_val_batch)

		self.cur_index_val = self.index_val_batch
		self.cur_index_train = 0
		self.val_split = self.index_val_batch
		self.len_batch = len(self.list_batchs)
		print("len batch", self.len_batch)

                # real data
                self.real_list_batchs = self.real_data.get_list_batch(batch_size=batch_real_size)
                #cv2.imshow(self.real_list_batchs[0]["label"][10], self.real_list_batchs[0]["data"][10])
                #cv2.waitKey(0)
                self.real_index_val_batch = int(len(self.real_list_batchs)*ratio_real_val)
                print("Index real val: ", self.real_index_val_batch)

                self.real_cur_index_val = self.real_index_val_batch
                self.real_cur_index_train = 0
                self.real_val_split = self.real_index_val_batch
                self.real_len_batch = len(self.real_list_batchs)
                print("Len real batch: ", self.real_len_batch)

    def get_real_len_batch(self):
        return self.real_len_batch

    def get_fake_len_batch(self):
        return self.len_batch

    def get_output_size(self):

		return len(alphabet)+1

    def get_batch(self, index, real_index):

		batch_data = self.list_batchs[index]
		imgs = batch_data["data"] #list
		source_str = batch_data["label"] #list
		size = len(imgs)
                #imgs = noise_aug.augment_images(imgs)
		datas = np.array(imgs)
		#print("Shape batch: ", datas.shape)
		labels = np.ones([size, self.absolute_max_len])*-1
		input_length = np.zeros([size, 1])
		label_length = np.zeros([size, 1])

                #real data
                batch_real_data = self.real_list_batchs[real_index]
                real_imgs = batch_real_data["data"]
                real_source_str = batch_real_data["label"]
                real_size = len(real_imgs)
                real_datas = np.array(real_imgs)

                real_labels = np.ones([real_size, self.absolute_max_len])*-1
                real_input_length = np.zeros([real_size, 1])
                real_label_length = np.zeros([real_size, 1])

		for i, label in enumerate(source_str):

			labels[i,:len(source_str[i])] = text_to_labels(source_str[i])
	    	        input_length[i] = self.img_w / self.downsample_factor - 1
	    	        label_length[i] = len(source_str[i])
	    	        #cv2.imwrite("img{}.jpg".format(label), imgs[i])
                #exit()

                for i, label in enumerate(real_source_str):
                    #print label, "--", len(label)
                    real_labels[i,:len(label)] = text_to_labels(label)
                    real_input_length[i] = self.img_w / self.downsample_factor-1
                    real_label_length[i] = len(label)

                datas = np.concatenate((datas, real_datas))
                labels = np.concatenate((labels, real_labels))
                input_length = np.concatenate((input_length, real_input_length))
                label_length = np.concatenate((label_length, real_label_length))
                source_str += real_source_str
                imgs += real_imgs

		inputs = {
			"the_input": datas,
			"the_labels":labels,
			"input_length": input_length,
			"label_length": label_length,
			"source_str": source_str,
			"images": imgs
			}

		outputs = {"ctc": np.zeros([size+real_size])}

		return (inputs, outputs)

    def next_train(self):

		print "TRAINBNNN"
		while 1:
			#print "heelpp"
			ret = self.get_batch(self.cur_index_train, self.real_cur_index_train)
			self.cur_index_train += 1
                        self.real_cur_index_train += 1

			if self.cur_index_train >= self.val_split:
				self.cur_index_train = 0

                        if self.real_cur_index_train >= self.real_val_split:
                            self.real_cur_index_train = 0
			yield ret

    def next_val(self):

		while 1:

			ret = self.get_batch(self.cur_index_val, self.real_cur_index_val)
			self.cur_index_val += 1
                        self.real_cur_index_val += 1

			if self.cur_index_val >= self.len_batch:
				self.cur_index_val = self.val_split

                        if self.real_cur_index_val >= self.real_len_batch:
                            self.real_cur_index_val = self.real_val_split

			yield ret

# the actual loss calc occurs here despite it not being
# an internal Keras loss function

def ctc_lambda_func(args):
	y_pred, labels, input_length, label_length = args
	return K.ctc_batch_cost(labels, y_pred, input_length, label_length)

def ctc_decoder(args):
	y_pred, input_length = args
	seq_len = tf.squeeze(input_length, axis = 1)
	return K.ctc_decode(y_pred = y_pred, input_length= seq_len, greedy= True, beam_width=100, top_paths = 1)[0]


# For a real OCR application, this should be beam search with a dictionary
# and language model.  For this example, best path is sufficient.

def decode_batch(test_func, word_batch):
	out = test_func([word_batch, 0])[0]
	ret = []
	for j in range(out.shape[0]):
		out_best = list(np.argmax(out[j], 1))
		out_best = [k for k, g in itertools.groupby(out_best)]
		outstr = labels_to_text(out_best)
		ret.append(outstr)
	return ret


class VizCallback(keras.callbacks.Callback):

	def __init__(self, run_name, test_func, text_img_gen, num_display_words=12, decode_func = None, clean=True):

		self.clean = clean
		self.test_func = test_func
		self.decode_func = decode_func
		self.output_dir = OUT_DIR +  "/" + run_name
		self.bestOutDir = OUT_BEST_MODEL
		##print (self.output_dir, 'test')
		self.text_img_gen = text_img_gen
		self.num_display_words = num_display_words
		self.bestLoss = lossMin
		self.currLoss = None

		if not os.path.exists(self.output_dir):
			os.makedirs(self.output_dir)
                if not os.path.exists(self.bestOutDir):
                    os.makedirs(self.bestOutDir)

	def show_edit_distance(self, num):
		num_left = num
		mean_norm_ed = 0.0
		mean_ed = 0.0
		acc_test = 0.0
		num_test = 0
		#print "Show edit distance"
		while num_left > 0:
			word_batch = next(self.text_img_gen)[0]
			#print "Shape test: ", word_batch['the_input'].shape
			num_proc = min(word_batch['the_input'].shape[0], num_left)
			num_test += num_proc
			# #print("num_proc", num_proc)
			decoded_res = decode_batch(self.test_func, word_batch['the_input'][0:num_proc])
			for j in range(num_proc):
				edit_dist = editdistance.eval(decoded_res[j], word_batch['source_str'][j])
				mean_ed += float(edit_dist)
				mean_norm_ed += float(edit_dist) / len(word_batch['source_str'][j])
				if edit_dist == 0:
					acc_test += 1.0

			num_left -= num_proc
		mean_norm_ed = mean_norm_ed / num
		mean_ed = mean_ed / num
		self.currLoss = mean_ed
		if self.clean:
			hist_val_clean_loss.append(mean_ed)
			acc_test_clean.append(acc_test/num_test)
                        #print acc_test_clean
		else:
			hist_val_noise_loss.append(mean_ed)
			acc_test_noise.append(acc_test/num_test)
                        #print acc_test_noise

		print('\nOut of %d samples:  Mean edit distance: %.3f Mean normalized edit distance: %0.3f'
			  % (num, mean_ed, mean_norm_ed))

	def on_epoch_end(self, epoch, logs={}):
		K.set_learning_phase(False)
		print "On epoch end"
                #print logs

		if self.clean :
			acc_train_clean.append(copy.deepcopy(logs))
		else:
			acc_train_noise.append(copy.deepcopy(logs))
                #print acc_train_noise

		self.show_edit_distance(1000)
		if epoch % 8 == 0:
			word_batch = next(self.text_img_gen)[0]
			res = decode_batch(self.test_func, word_batch['the_input'][0:self.num_display_words])
			predicted = self.test_func([word_batch['the_input'][:self.num_display_words], 0])[0]
			# #print(predicted.shape)
			ctc_res = self.decode_func([predicted, word_batch["input_length"][:self.num_display_words]])[0]
			for i in range(self.num_display_words):
				print("Truth = '%s' \tDecoded = '%s'\tCtc_decoder = '%s'" % (word_batch['source_str'][i], res[i], labels_to_text(ctc_res[i])))


		self.model.save_weights(os.path.join(self.output_dir, 'weights%02d.h5' % (epoch)))
		# if self.bestLoss > self.currLoss:
		# 	self.model.save_weights(os.path.join(self.bestOutDir, 'weights%02d.h5' % (epoch)))
		# 	self.bestLoss = self.currLoss


def train(run_name, start_epoch, stop_epoch, img_w, fine_tune_part = False, clean=True, good=True):
	# Input Parameters
	img_h = 32
	words_per_epoch = 16000
	val_split = 0.2
	val_words = int(words_per_epoch * (val_split))

	input_shape = (img_h, img_w, 3)
	minibatch_size = 32
        real_minibatch_size = 16

	pool_size = 2
        ratio_val = 0.8
        ratio_real_val=0.7
	img_gen = TextImageGenerator(ratio_val = ratio_val, ratio_real_val=ratio_real_val, batch_size=minibatch_size, batch_real_size=real_minibatch_size, downsample_factor=pool_size**3, img_h=img_h, img_w=img_w, clean=clean, good=good)
        len_batch = img_gen.get_fake_len_batch()
        real_len_batch = img_gen.get_real_len_batch()

        tensor_board = keras.callbacks.TensorBoard(log_dir='./logs', histogram_freq=0, batch_size=32, write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_metadata=None, update_freq='epoch')

	input_data = Input(name='the_input', shape=input_shape, dtype='float32')
	inner = Conv2D(64, (3,3),strides = (1,1), padding='same', activation="relu", kernel_initializer='he_normal', name='conv1')(input_data)
	inner = MaxPooling2D(pool_size=(2, 2), strides=(2,2), name='max1')(inner)
	inner = Conv2D(128, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv2')(inner)
	inner = MaxPooling2D(pool_size=(2, 2),strides=(2,2), name='max2')(inner)
	inner = Conv2D(256, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv3')(inner)
	inner = Conv2D(256, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv4')(inner)
	inner = MaxPooling2D(pool_size=(2, 2),strides=(2,2), name='max3')(inner)
	inner = Dropout(0.2, name = "dropout1")(inner)
	inner = Conv2D(512, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv5')(inner)
	inner = BatchNormalization(name="batch_norm1")(inner)
	inner = Dropout(0.2, name = "dropout2")(inner)
	inner = Conv2D(512, (3,3),strides = (1,1), padding='same',activation="relu", kernel_initializer='he_normal',name='conv6')(inner)
	inner = BatchNormalization(name="batch_norm2")(inner)
	inner = MaxPooling2D(pool_size=(2, 1),strides=(2,1), name='max4')(inner)

	CNN_out = Conv2D(512, (2,2),strides = (1,1),activation="relu", kernel_initializer='he_normal',name='conv7')(inner)

	# Model(inputs=input_data, outputs=inner).summary()
	conv_to_rnn_dims = (img_w // (2 ** 3) - 1,512)

	inner = Reshape(target_shape=conv_to_rnn_dims, name='reshape')(CNN_out)

	lstm1 = LSTM(256, return_sequences=True, kernel_initializer="he_normal", name='lstm1', dropout=0.4)(inner)
	lstm1b = LSTM(256, return_sequences=True, go_backwards=True, kernel_initializer="he_normal", name='lstm1b', dropout=0.4)(inner)

	merge_concat = concatenate([lstm1, lstm1b])

	inner = Dense(img_gen.get_output_size(), kernel_initializer="he_normal", name="Dense")(merge_concat)
	y_pred = Activation("softmax", name="softmax")(inner)

	Model(inputs=input_data, outputs=y_pred).summary()

	labels = Input(name='the_labels', shape=[img_gen.absolute_max_len], dtype='float32')
	input_length = Input(name='input_length', shape=[1], dtype='int64')
	label_length = Input(name='label_length', shape=[1], dtype='int64')
	# Keras doesn't currently support loss funcs with extra parameters
	# so CTC loss is implemented in a lambda layer
	loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([y_pred, labels, input_length, label_length])

	# clipnorm seems to speeds up convergence
	sgd = SGD(lr=0.005, decay=1e-8, momentum=0.9, nesterov=True, clipnorm=5)
	#ada = Adadelta(decay=1e-8)
	adam = Adam(lr=0.005)

	model = Model(inputs=[input_data, labels, input_length, label_length], outputs=loss_out)

	# the loss calc occurs elsewhere, so use a dummy lambda func for the loss
	model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=adam)
	if start_epoch > 0:
	    weight_file = os.path.join(OUT_DIR, os.path.join(run_name, 'weights%02d.h5' % (start_epoch-1)))


            # If fine_tune_part == True->just load part of model
	    if not fine_tune_part :
                model.load_weights(weight_file, True)
	    else:
                CNN = Model(input_data, CNN_out)
                CNN.load_weights(weight_file, True)

	test_func = K.function([input_data, K.learning_phase()], [y_pred])
	decoder = Lambda(ctc_decoder, output_shape = [None, ], name="decoder")([y_pred, input_length])
	decode_func = K.function([y_pred, input_length], [decoder])

        # img_gen.next_val() call in 2 place so will at one time 2 place also call next_val() -> no data to val
	viz_cb = VizCallback(run_name, test_func, img_gen.next_val(), decode_func = decode_func, clean=clean)

        steps = max(int(len_batch*ratio_val), int(real_len_batch*ratio_real_val))
        nums = max(int(len_batch), int(real_len_batch))
	model.fit_generator(generator=img_gen.next_train(),
						steps_per_epoch=steps,
						epochs=stop_epoch,
						validation_data=img_gen.next_val(),
						validation_steps=nums - steps,
						callbacks=[viz_cb, img_gen],
						initial_epoch=start_epoch)

from Recognizor import Recognizor
import pickle
if __name__ == '__main__':
        clean = True
	run_name = "" #"20180226151024" #datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        if clean:
            train(run_name, 0, 100, 223, clean=clean)
        else:
            OUT_DIR = 'model_id_real_noise'
            train(run_name, 0, 100, 223, clean=False)

        if clean:
            f = open("hist_val_clean.txt", 'w')
	    pickle.dump(hist_val_clean_loss, f)
        else:
            f1 = open("real_hist_val_noise.txt", 'w')
	    pickle.dump(hist_val_noise_loss, f1)

        if clean:
            f2 = open("acc_test_clean.txt", 'w')
	    pickle.dump(acc_test_clean, f2)
        else:
            f3 = open("real_acc_test_noise.txt", "w")
	    pickle.dump(acc_test_noise, f3)

        if clean:
            f4 = open("acc_train_clean.txt", 'w')
	    pickle.dump(acc_train_clean, f4)
        else:
	    f5 = open("real_acc_train_noise.txt", 'w')
	    pickle.dump(acc_train_noise, f5)

	#img_gen = TextImageGenerator(0.7, downsample_factor=2**3, img_h=32, img_w=232, clean=True)
	#while 1:
	    #img_gen.next_train()
